package pt.uminho.merstel.alm.peer.agent;

import pt.uminho.merstel.alm.peer.utils.NetworkUtils;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class NeighbourManagementAgent {

    private final List<InetAddress> groupElements;
    private final Map<Integer, InetAddress> groupHeads;
    private final List<InetAddress> connections;

    private int groupId;
    private InetAddress groupHead;
    private boolean isGroupHead = true;

    private PeerAgent agent;

    public NeighbourManagementAgent(PeerAgent agent) {
        this.agent = agent;
        this.groupId = -1;
        this.groupHead = InetAddress.getLoopbackAddress();

        this.groupElements = new ArrayList<>();
        this.groupHeads = new ConcurrentHashMap<>();
        this.connections = new ArrayList<>();
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public void setGroupHead(InetAddress groupHead) {
        this.groupHead = groupHead;
        this.isGroupHead = NetworkUtils.isLocal(groupHead);
    }

    public InetAddress getGroupHead() {
        return groupHead;
    }

    public boolean isGroupHead (){
        return isGroupHead;
    }

    public void removePeer(InetAddress address) {
        this.groupElements.remove(address);
        this.groupHeads.remove(address);
        this.connections.remove(address);
    }

    // --------- OTHER GROUP HEADS -------------

    public void setGroupHeads(Map<Integer, InetAddress> groupHeads) {
        this.groupHeads.clear();
        this.groupHeads.putAll(groupHeads);
    }

    public Map<Integer, InetAddress> getGroupHeads() {
        return new HashMap<>(this.groupHeads);
    }

    public Collection<InetAddress> getOtherGroupHeadsList(InetAddress address) {
        Map<Integer, InetAddress> aux = new HashMap<>(groupHeads);
        aux.values().removeIf(a -> a.equals(address));
        return aux.values();
    }

    public Map<Integer, InetAddress> getOtherGroupHeads() {
        Map<Integer, InetAddress> aux = new HashMap<>(groupHeads);
        aux.keySet().removeIf(g -> g == groupId);
        return aux;
    }

    // ------------ GROUP ELEMENTS --------------

    public void setGroupElements(List<InetAddress> groupElements) {
        this.groupElements.clear();
        this.groupElements.addAll(groupElements);
    }

    public List<InetAddress> getGroupElements() {
        return new ArrayList<>(this.groupElements);
    }

    public List<InetAddress> getOtherGroupElements() {
        List<InetAddress> aux = new ArrayList<>(groupElements);
        aux.removeIf(p -> NetworkUtils.isLocal(p));
        return aux;
    }

    // ------------ CONNECTIONS -------------

    public void setConnections(Collection<InetAddress> connections) {
        this.connections.clear();
        this.connections.addAll(connections);
    }

    public List<InetAddress> getConnections() {
        return new ArrayList<>(this.connections);
    }

    public List<InetAddress> getOtherConnections(InetAddress address) {
        List<InetAddress> list = new ArrayList<>(this.connections);
        list.removeIf(a -> a.equals(address));
        return list;
    }

}
