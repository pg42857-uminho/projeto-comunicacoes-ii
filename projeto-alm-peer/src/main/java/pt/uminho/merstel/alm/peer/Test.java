package pt.uminho.merstel.alm.peer;

import pt.uminho.merstel.alm.peer.configuration.GroupsConfiguration;
import pt.uminho.merstel.alm.peer.control.ControlClient;
import pt.uminho.merstel.alm.peer.graph.Edge;
import pt.uminho.merstel.alm.peer.graph.Kruskal;
import pt.uminho.merstel.alm.peer.graph.TreeChart;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {

    public static void main(String[] args) throws IOException {
        /*InetAddress address2 = InetAddress.getByName("10.0.11.20");

        GroupsConfiguration instance = GroupsConfiguration.loadInstance("C:\\Users\\danie\\OneDrive\\Ambiente de Trabalho\\Projeto Comunicações II\\projeto-alm\\projeto-alm-peer\\groups-configuration.cfg");
        int groupId = instance.getGroup(address2);

        System.out.println(groupId);*/
        testDiffGroups();
    }

    private static void testDiffGroups() throws UnknownHostException {
        Map<Integer, List<Edge>> connections = new HashMap<>();
        Map<Integer, List<InetAddress>> peers = new HashMap<>();

        InetAddress address0 = InetAddress.getByName("10.0.9.20");
        InetAddress address1 = InetAddress.getByName("10.0.11.20");
        InetAddress address2 = InetAddress.getByName("10.0.12.20");

        List<InetAddress> allAddress = new ArrayList<>();
        allAddress.add(address0);
        allAddress.add(address1);
        allAddress.add(address2);

        List<InetAddress> group1 = new ArrayList<>();
        group1.add(address0);

        List<InetAddress> group2 = new ArrayList<>();
        group2.add(address1);
        group2.add(address2);

        List<Edge> group0Connections = new ArrayList<>();
        group0Connections.add(new Edge(address0, address2, 2));

        List<Edge> group1Connections = new ArrayList<>();
        //group1Connections.add(new Edge(address2, address1, 1));

        peers.put(1, group1);
        peers.put(2, group2);

        connections.put(0, group0Connections);
        connections.put(2, group1Connections);

        Map<Integer, InetAddress> groupHeads = new HashMap<>();

        long ts0 = System.currentTimeMillis();
        Map<Integer, List<Edge>> result = Kruskal.computeWholeTree(
                connections, peers, address0, groupHeads);

        long ts1 = System.currentTimeMillis();

        System.out.println(ts1 - ts0);

        ControlClient controlClient = new ControlClient(null);
        controlClient.sendTreeAdvertisements(allAddress, groupHeads, result);

        /*List<Edge> allEdges = new ArrayList<>();
        result.entrySet().forEach(e -> {
            e.getValue().entrySet().forEach(o -> {
                Edge edge = new Edge(o.getKey().getFirst(), o.getKey().getSecond(), o.getValue());
                allEdges.add(edge);
            });
        });*/

        //TreeChart treeChart = new TreeChart();
        //treeChart.generateTreeChart(allEdges);

        //GroupsConfiguration configuration = GroupsConfiguration.loadInstance("C:\\Users\\danie\\OneDrive\\Ambiente de Trabalho\\Projeto Comunicações II\\projeto-alm\\projeto-alm-peer\\groups-configuration.cfg");
        //int group = configuration.getGroup(InetAddress.getByName("10.0.10.1"));

        //System.out.println(group);
    }

    private static void testSmall() throws UnknownHostException {
        Map<Integer, List<Edge>> connections = new HashMap<>();
        Map<Integer, List<InetAddress>> peers = new HashMap<>();

        InetAddress rpAddress = InetAddress.getByName("10.0.9.20");
        InetAddress address1 = InetAddress.getByName("10.0.10.20");

        List<InetAddress> group1 = new ArrayList<>();
        group1.add(rpAddress);
        group1.add(address1);

        List<Edge> group1Connections = new ArrayList<>();
        group1Connections.add(new Edge(rpAddress, address1, 21));

        peers.put(1, group1);
        connections.put(0, new ArrayList<>());
        connections.put(1, group1Connections);

        Map<Integer, InetAddress> groupHeads = new HashMap<>();

        long ts0 = System.currentTimeMillis();
        Map<Integer, List<Edge>> result = Kruskal.computeWholeTree(connections, peers, rpAddress, groupHeads);

        long ts1 = System.currentTimeMillis();

        System.out.println(ts1 - ts0);

        /*List<Edge> allEdges = new ArrayList<>();
        result.entrySet().forEach(e -> {
            e.getValue().entrySet().forEach(o -> {
                Edge edge = new Edge(o.getKey().getFirst(), o.getKey().getSecond(), o.getValue());
                allEdges.add(edge);
            });
        });*/

        //TreeChart treeChart = new TreeChart();
        //treeChart.generateTreeChart(allEdges);

        //GroupsConfiguration configuration = GroupsConfiguration.loadInstance("C:\\Users\\danie\\OneDrive\\Ambiente de Trabalho\\Projeto Comunicações II\\projeto-alm\\projeto-alm-peer\\groups-configuration.cfg");
        //int group = configuration.getGroup(InetAddress.getByName("10.0.10.1"));

        //System.out.println(group);
    }

    private static void testBig() throws UnknownHostException {
        Map<Integer, List<Edge>> connections = new HashMap<>();
        Map<Integer, List<InetAddress>> peers = new HashMap<>();
        InetAddress rpAddress = InetAddress.getByName("192.168.1.254");

        InetAddress address0 = InetAddress.getByName("192.168.1.0");
        InetAddress address1 = InetAddress.getByName("192.168.1.1");
        InetAddress address2 = InetAddress.getByName("192.168.1.2");
        InetAddress address3 = InetAddress.getByName("192.168.1.3");
        InetAddress address4 = InetAddress.getByName("192.168.1.4");
        InetAddress address5 = InetAddress.getByName("192.168.1.5");
        InetAddress address6 = InetAddress.getByName("192.168.1.6");
        InetAddress address7 = InetAddress.getByName("192.168.1.7");
        InetAddress address8 = InetAddress.getByName("192.168.1.8");
        InetAddress address9 = InetAddress.getByName("192.168.1.9");

        List<InetAddress> group1 = new ArrayList<>();
        group1.add(address0);
        group1.add(address6);
        group1.add(address7);
        group1.add(address8);
        group1.add(address9);

        List<Edge> group1Connections = new ArrayList<>();
        group1Connections.add(new Edge(address0, address6, 20));
        group1Connections.add(new Edge(address0, address7, 20));
        group1Connections.add(new Edge(address0, address8, 20));
        group1Connections.add(new Edge(address7, address9, 20));

        List<InetAddress> group2 = new ArrayList<>();
        group2.add(address2);
        group2.add(address3);
        group2.add(address4);
        group2.add(address5);

        List<Edge> group2Connections = new ArrayList<>();
        group2Connections.add(new Edge(address2, address3, 20));
        group2Connections.add(new Edge(address2, address4, 20));
        group2Connections.add(new Edge(address2, address5, 20));

        List<InetAddress> group3 = new ArrayList<>();
        group3.add(address1);

        peers.put(1, group1);
        peers.put(2, group2);
        peers.put(3, group3);

        connections.put(1, group1Connections);
        connections.put(2, group2Connections);

        Map<Integer, InetAddress> groupHeads = new HashMap<>();
        Map<Integer, List<Edge>> result = Kruskal.computeWholeTree(connections, peers, rpAddress, groupHeads);

        //TreeChart treeChart = new TreeChart();
        //treeChart.generateTreeChart(result);

        //GroupsConfiguration configuration = GroupsConfiguration.loadInstance("C:\\Users\\danie\\OneDrive\\Ambiente de Trabalho\\Projeto Comunicações II\\projeto-alm\\projeto-alm-peer\\groups-configuration.cfg");
        //int group = configuration.getGroup(InetAddress.getByName("10.0.10.1"));

        //System.out.println(group);
    }

}
