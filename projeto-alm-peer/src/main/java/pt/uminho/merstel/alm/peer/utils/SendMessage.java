package pt.uminho.merstel.alm.peer.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class SendMessage {

    private static Logger logger = LogManager.getLogger(SendMessage.class);

    private static volatile SendMessage instance;
    private static Object mutex = new Object();

    public static SendMessage getInstance() {
        if (instance == null) {
            synchronized (mutex) {
                if (instance == null) {
                    instance = new SendMessage();
                }
            }
        }
        return instance;
    }

    private ThreadPoolExecutor threadPoolExecutor;

    private SendMessage() {
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
    }

    public void sendMessageTCP(InetAddress address, int portNumber, byte[] content) {
        Runnable runnable = () -> sendMessageTCP0(address, portNumber, content);
        threadPoolExecutor.execute(runnable);
    }

    public void sendMessageUDP(InetAddress address, int portNumber, byte[] content) {
        Runnable runnable = () -> sendMessageUDP0(address, portNumber, content);
        threadPoolExecutor.execute(runnable);
    }

    private void sendMessageTCP0(InetAddress address,
                                 int portNumber,
                                 byte[] content) {
        try {
            try (Socket clientSocket = new Socket(address, portNumber);
                 DataOutputStream dOut = new DataOutputStream(clientSocket.getOutputStream());) {

                dOut.write(content);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendMessageUDP0(InetAddress address,
                                 int portNumber,
                                 byte[] buffer) {
        try (DatagramSocket socket = new DatagramSocket()) {
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, portNumber);
            socket.send(packet);

        } catch (IOException e) {
            logger.error(e);
        }
    }

}
