package pt.uminho.merstel.alm.peer.configuration;

import org.apache.commons.net.util.SubnetUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.utils.Constants;
import pt.uminho.merstel.alm.peer.utils.DataUtils;
import pt.uminho.merstel.alm.peer.utils.Pair;

import javax.swing.*;
import java.io.*;
import java.net.InetAddress;
import java.util.*;
import java.util.function.Function;

public class GroupsConfiguration {

    private static Logger logger = LogManager.getLogger(GroupsConfiguration.class);

    // ------------------ SINGLETON ------------

    private static GroupsConfiguration instance;

    private GroupsConfiguration() {}

    public static GroupsConfiguration loadInstance(String filename) throws IOException {
        instance = load(filename);
        return instance;
    }

    public static GroupsConfiguration getInstance() {
        return instance;
    }

    // ------------------- INSTANCE ----------------

    private Map<Pair<String, Integer>, Integer> groupTable;

    private GroupsConfiguration(Map<Pair<String, Integer>, Integer> groupTable) {
        this.groupTable = groupTable;
    }

    public int getGroup(InetAddress address) {
        for (Map.Entry<Pair<String, Integer>, Integer> entry : groupTable.entrySet()) {
            Pair<String, Integer> entryPair = entry.getKey();

            boolean match = match(entryPair, address);

            if (match) {
                return entry.getValue();
            }
        }

        return -1;
    }

    private boolean match(Pair<String, Integer> entry, InetAddress address) {
        return match0(entry, address);
    }

    private boolean match0(Pair<String, Integer> entry, InetAddress address) {
        String cidr = entry.getFirst();   // 10.1.1.0/24

        SubnetUtils subnetUtils = new SubnetUtils(cidr);
        subnetUtils.setInclusiveHostCount(true);

        return subnetUtils.getInfo().isInRange(address.getHostAddress());
    }

    // -------------------- LOAD CONFIGS --------------------------

    private static boolean validate(String filename) {
        try {
            File file = new File(filename);
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.trim().isEmpty()) {
                    String[] aux0 = line.trim().split("=");
                    if (aux0.length == 2) {
                        Integer group = Integer.parseInt(aux0[0].trim());

                        if (group < 1) {
                            logger.info("ERROR: found a negative or zero group id");
                            return false;
                        }

                        String[] values = aux0[1].trim().split(",");

                        for (String value : values) {
                            if (!Constants.CIDR_PATTERN.matcher(value).matches()) {
                                logger.info("ERROR: invalid format " + value);
                                return false;
                            }
                        }

                    } else {
                        logger.info("ERROR: found invalid non-empty line");
                        return false;
                    }
                }
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static GroupsConfiguration load(String filename) throws IOException {
        Comparator<Pair<String, Integer>> comparator =
                (o1, o2) -> Comparator.comparing((Function<Pair<String, Integer>, Integer>) Pair::getSecond)
                        .thenComparing(Pair::getFirst)
                        .compare(o2, o1);

        Map<Pair<String, Integer>, Integer> groupTable = new TreeMap<>(comparator);

        File file = new File(filename);
        BufferedReader reader = new BufferedReader(new FileReader(file));

        String line;
        while ((line = reader.readLine()) != null) {
            if (!line.trim().isEmpty()) {
                String[] aux0 = line.trim().split("=");

                Integer group = Integer.parseInt(aux0[0].trim());
                String[] values = aux0[1].trim().split(",");

                for (String value : values) {
                    try {
                        Pair<String, Integer> addressMaskValue = parseValue(value);
                        groupTable.put(addressMaskValue, group);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            }
        }

        return new GroupsConfiguration(groupTable);
    }

    private static Pair<String, Integer> parseValue(String value) throws Exception {
        if ("*".equals(value)) {
            return new Pair<>("0.0.0.0/0", 0);
        } else {
            String[] aux = value.trim().split("/");

            String address = aux[0];
            String mask = aux[1];

            int i = DataUtils.parseToInt(address);
            int m = Integer.parseInt(mask);

            return new Pair<>(value, m);
        }
    }

}
