package pt.uminho.merstel.alm.peer.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.utils.Constants;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ControlServer extends Thread {

    private static final Logger logger = LogManager.getLogger(ControlServer.class);

    private ThreadPoolExecutor threadPoolExecutor;
    private volatile Configuration configuration;
    private volatile PeerAgent agent;

    private boolean running = true;

    public ControlServer(PeerAgent agent, Configuration configuration) {
        this.agent = agent;
        this.configuration = configuration;
        this.threadPoolExecutor = (ThreadPoolExecutor) Executors
                        .newScheduledThreadPool(Constants.CONTROL_SERVER_THREAD_NUMBER);
    }

    @Override
    public void run() {
        int portNumber = Constants.CONTROL_PORT_NUMBER;

        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
            logger.info("ALM Control Server listening on: " + portNumber);

            while (running) {
                this.threadPoolExecutor.execute(
                        new ControlHandler(
                                agent,
                                serverSocket.accept()));
            }
        } catch (IOException e) {
            logger.error("Could not listen on port " + portNumber);
        }
    }

    public void stopRunning() {
        this.running = false;
    }

}
