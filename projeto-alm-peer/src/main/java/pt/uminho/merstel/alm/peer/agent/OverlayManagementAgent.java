package pt.uminho.merstel.alm.peer.agent;

import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.configuration.GroupsConfiguration;
import pt.uminho.merstel.alm.peer.graph.Edge;
import pt.uminho.merstel.alm.peer.utils.Pair;

import java.net.InetAddress;
import java.util.*;

/**
 * - list of every peer in the overlay network
 * - list of every group head in the overlay network
 * - updated topology of the overlay network
 * - list of every connection and metric
 */
public class OverlayManagementAgent {

    private static final int LEVEL1_GROUP = 0;

    private final Map<Integer, List<InetAddress>> allPeers;
    private final Map<Integer, InetAddress> allGroupHeads;
    /**
     * Map<GroupId, Map<Edge, Integer>>
     */
    private final Map<Integer, List<Edge>> topology;
    /**
     * Map<GroupId, Map<Edge, Integer>>
     */
    private final Map<Integer, List<Edge>> allConnections;

    private GroupsConfiguration groupsConfiguration;

    private boolean changeFlag = false;

    public OverlayManagementAgent(GroupsConfiguration groupsConfiguration) {
        this.groupsConfiguration = groupsConfiguration;

        this.allPeers = new HashMap<>();
        this.allGroupHeads = new HashMap<>();
        this.topology = new HashMap<>();
        this.allConnections = new HashMap<>();
        this.allConnections.put(LEVEL1_GROUP, new ArrayList<>());
    }

    public boolean getChangeFlag() {
        return changeFlag;
    }

    public void resetFlag() {
        changeFlag = false;
    }

    // ------- PEERS -------

    public void addPeer(InetAddress address) {
        int groupId = groupsConfiguration.getGroup(address);
        addPeer(address, groupId);
    }

    public void addPeer(InetAddress address, int groupId) {
        if (allPeers.containsKey(groupId)) {
            List<InetAddress> addressList = allPeers.get(groupId);

            if (!addressList.contains(address)){
                addressList.add(address);
            }
        }
        else {
            List<InetAddress> addressList = new ArrayList<>();
            addressList.add(address);

            allPeers.put(groupId, addressList);
        }

        this.allGroupHeads.putIfAbsent(groupId, address);
    }

    public boolean hasPeer(InetAddress address) {
        int groupId = groupsConfiguration.getGroup(address);

        if (allPeers.containsKey(groupId)) {
            return allPeers.get(groupId).contains(address);
        }

        return false;
    }

    public List<InetAddress> getPeers(int groupId) {
        List<InetAddress> peers = this.allPeers.get(groupId);

        if (peers == null) {
            peers = new ArrayList<>();
        }

        return new ArrayList<>(peers);
    }

    public void removePeer(InetAddress address) {
        this.allPeers.values().forEach(l -> l.removeIf(
                a -> a.equals(address)));

        this.allGroupHeads.values().removeIf(a -> a.equals(address));

        this.allConnections.values().forEach(l -> l.removeIf(
                e -> e.source.equals(address) || e.destination.equals(address)));

        this.topology.values().forEach(l -> l.removeIf(
                e -> e.source.equals(address) || e.destination.equals(address)));
    }

    // ------ GHs ------

    public void addGroupHead(InetAddress address) {
        int groupId = groupsConfiguration.getGroup(address);

        if (!allPeers.containsKey(groupId) || !allPeers.get(groupId).contains(address)) {
            addPeer(address, groupId);
        }

        allGroupHeads.put(groupId, address);
    }

    public Map<Integer, InetAddress> getGroupHeads() {
        return new HashMap<>(this.allGroupHeads);
    }


    public InetAddress getGroupHead(int groupId) {
        return this.allGroupHeads.get(groupId);
    }

    // ---- CONNECTIONS ----

    public void addConnection(InetAddress address0, InetAddress address1, int metric) {
        addPeer(address0);
        addPeer(address1);

        int group0 = groupsConfiguration.getGroup(address0);
        int group1 = groupsConfiguration.getGroup(address1);

        int group = group0;

        if (group0 != group1) {
            group = 0;
        }

        if (allConnections.containsKey(group)) {
            allConnections.get(group).add(new Edge(address0, address1, metric));
        }
        else {
            List<Edge> auxList = new ArrayList<>();
            auxList.add(new Edge(address0, address1, metric));
            allConnections.put(group, auxList);
        }
    }

    public void updateConnections(InetAddress address,
                                  List<Edge> peerConnections) {
        int groupId = groupsConfiguration.getGroup(address);

        if (allConnections.containsKey(groupId)) {
            allConnections.get(groupId).removeIf(p -> p.source.equals(address) || p.destination.equals(address));
        }
        else {
            allConnections.put(groupId, new ArrayList<>());
        }

        List<Edge> groupConnections = allConnections.get(groupId);

        boolean isGroupHead = allGroupHeads.containsKey(groupId) && allGroupHeads.get(groupId).equals(address);

        for (Edge edge : peerConnections) {
            boolean level1 = false;

            if (isGroupHead) {
                // THE SECOND VALUE HAS THE ADDRESS OF THE OTHER PEER!!!!
                int otherGroupId = groupsConfiguration.getGroup(edge.destination);
                level1 = otherGroupId != groupId && allGroupHeads.containsValue(edge.source);
            }

            if (level1) {
                allConnections.get(LEVEL1_GROUP).add(edge);
            }
            else {
                groupConnections.add(edge);
            }
        }
    }

    public Map<Integer, List<Edge>> getAllConnections() {
        return this.allConnections;
    }

    public Map<Integer, List<InetAddress>> getAllPeers() {
        return this.allPeers;
    }

    public List<InetAddress> getAllPeersList() {
        List<InetAddress> list = new ArrayList<>();
        this.allPeers.entrySet().forEach(p -> list.addAll(p.getValue()));
        return list;
    }

    public void updateTopology(Map<Integer, List<Edge>> newTopology,
                               Map<Integer, InetAddress> groupHeads) {
        this.topology.clear();
        this.topology.putAll(newTopology);

        this.allGroupHeads.clear();
        this.allGroupHeads.putAll(groupHeads);
    }

}
