package pt.uminho.merstel.alm.peer.utils;

import java.net.InetAddress;

public class ControlMessage {

    public InetAddress source;
    public byte pduType;
    public byte[] buffer;

    public ControlMessage(InetAddress source, byte pduType, byte[] buffer) {
        this.source = source;
        this.pduType = pduType;
        this.buffer = buffer;
    }

}
