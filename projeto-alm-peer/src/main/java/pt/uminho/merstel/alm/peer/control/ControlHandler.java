package pt.uminho.merstel.alm.peer.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.graph.Edge;
import pt.uminho.merstel.alm.peer.utils.ControlMessage;
import pt.uminho.merstel.alm.peer.utils.DataUtils;
import pt.uminho.merstel.alm.peer.utils.PDUType;
import pt.uminho.merstel.alm.peer.utils.Pair;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.*;

public class ControlHandler implements Runnable {

    private static Logger logger = LogManager.getLogger(ControlHandler.class);

    private volatile Socket socket;
    private volatile PeerAgent agent;

    public ControlHandler(PeerAgent agent, Socket socket) {
        this.agent = agent;
        this.socket = socket;
    }

    @Override
    public void run() {
        try (DataInputStream dIn = new DataInputStream(socket.getInputStream())) {
            byte[] buffer = dIn.readAllBytes();
            byte type = buffer[0];

            InetAddress source = socket.getInetAddress();

            ControlMessage controlMessage = new ControlMessage(source, type, buffer);

            switch (type) {
                case PDUType.HELLO:
                    logger.info("Peer received HELLO from " + source.getHostAddress() + "!");
                    break;
                case PDUType.JOIN_REQ:
                    logger.info("Peer received JOIN REQ from " + source.getHostAddress() + "!");
                    break;
                case PDUType.JOIN_ACK:
                    logger.info("Peer received JOIN ACK from " + source.getHostAddress() + "!");
                    break;
                case PDUType.PEER_UPDATE:
                    logger.info("Peer received PEER UPDATE from " + source.getHostAddress() + "!");
                    break;
                case PDUType.PEER_TIMEDOUT:
                    logger.info("Peer received PEER TIMEDOUT fom " + source.getHostAddress() + "!");
                    break;
                case PDUType.TREE_ADV:
                    logger.info("Peer received TREE ADV from " + source.getHostAddress() + "!");
                    break;
                default:
                    break;
            }

            agent.handleMessage(controlMessage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static synchronized Map<String, Object> handleJoinAcknowledgement(ControlMessage message) throws UnknownHostException {
        Map<String, Object> map = new HashMap<>();

        byte[] buffer = message.buffer;

        int groupId = buffer[1];
        int numberOfGroupElements = buffer[2];
        int numberOfGroupHeads = buffer[3];

        map.put("groupId", groupId);

        byte[] groupHeadAddressBytes = Arrays.copyOfRange(buffer, 4, 8);

        InetAddress groupHead = InetAddress.getByAddress(groupHeadAddressBytes);
        map.put("groupHead", groupHead);

        List<InetAddress> groupElements = new ArrayList<>();

        for (int i = 0; i < numberOfGroupElements; i++) {
            int initialIndex = 8 + (i * 4);
            byte[] addressBytes = Arrays.copyOfRange(buffer, initialIndex, initialIndex + 4);

            InetAddress address = InetAddress.getByAddress(addressBytes);
            groupElements.add(address);
        }

        map.put("groupElements", groupElements);

        Map<Integer, InetAddress> groupHeads = new HashMap<>();

        for (int i = 0; i < numberOfGroupHeads; i++) {
            int index = 8 + (numberOfGroupElements * 4) + (i * 8);

            byte[] groupBytes = Arrays.copyOfRange(buffer, index, index + 4);
            byte[] addressBytes = Arrays.copyOfRange(buffer, index + 4, index + 8);

            InetAddress address = InetAddress.getByAddress(addressBytes);
            int group = DataUtils.bytesToInt(groupBytes);
            groupHeads.put(group, address);
        }

        map.put("groupHeads", groupHeads);

        return map;
    }

    public static synchronized Map<String, Object> handlePeerUpdate(ControlMessage message) {
        Map<String, Object> map = new HashMap<>();

        byte[] buffer = message.buffer;
        InetAddress address = message.source;

        int length = buffer[1];

        List<Edge> connections = new ArrayList<>();

        for (int i = 0; i < length; i++) {
            byte[] addressBytes = new byte[4];

            for (int j = 0; j < 4; j++) {
                addressBytes[j] = buffer[4 + (i * 8) + j];
            }

            InetAddress otherAddress = null;
            try {
                otherAddress = InetAddress.getByAddress(addressBytes);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            byte[] metricBytes = new byte[4];

            for (int j = 0; j < 4; j++) {
                metricBytes[j] = buffer[4 + (i * 8) + 4 + j];
            }

            int metric = DataUtils.bytesToInt(metricBytes);

            connections.add(new Edge(address, otherAddress, metric));
        }

        map.put("connections", connections);

        return map;
    }

    public static synchronized Map<String, Object> handlePeerTimeout(ControlMessage message) {
        Map<String, Object> result = new HashMap<>();

        byte[] buffer = message.buffer;

        byte[] addressBytes = new byte[4];

        for (int i = 0; i < 4; i++) {
            addressBytes[i] = buffer[4+i];
        }

        try {
            InetAddress address = InetAddress.getByAddress(addressBytes);
            result.put("address", address);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static synchronized Map<String, Object> handleTreeAdvertisement(ControlMessage message) throws UnknownHostException {
        byte[] buffer = message.buffer;
        int numberOfGroups = buffer[1];

        Map<Integer, InetAddress> groupHeads = new HashMap<>();
        Map<Integer, List<InetAddress>> groupsElements = new HashMap<>();
        Map<Integer, List<Edge>> groupsConnections = new HashMap<>();

        int lastIndex = 4;

        for (int i = 0; i < numberOfGroups; i++) {
            List<InetAddress> elements = new ArrayList<>();
            List<Edge> connections = new ArrayList<>();

            int groupId = buffer[lastIndex];
            int numberOfConnections = buffer[lastIndex + 1];

            byte[] ghAddressBytes = Arrays.copyOfRange(buffer, lastIndex + 4, lastIndex + 8);
            InetAddress ghAddress = InetAddress.getByAddress(ghAddressBytes);

            groupHeads.put(groupId, ghAddress);

            for (int j = 0; j < numberOfConnections; j++) {
                int initialIndex = lastIndex + 8 + (j * 12);

                byte[] peerABytes = Arrays.copyOfRange(buffer, initialIndex, initialIndex + 4);
                byte[] peerBBytes = Arrays.copyOfRange(buffer, initialIndex + 4, initialIndex + 8);
                byte[] metricBytes = Arrays.copyOfRange(buffer, initialIndex + 8, initialIndex + 12);

                InetAddress peerAAddress = InetAddress.getByAddress(peerABytes);
                InetAddress peerBAddress = InetAddress.getByAddress(peerBBytes);
                int metric = DataUtils.bytesToInt(metricBytes);

                if (!elements.contains(peerAAddress)) {
                    elements.add(peerAAddress);
                }

                if (!elements.contains(peerBAddress)) {
                    elements.add(peerBAddress);
                }

                connections.add(new Edge(peerAAddress, peerBAddress, metric));
            }

            groupsElements.put(groupId, elements);
            groupsConnections.put(groupId, connections);

            lastIndex += 8 + (numberOfConnections * 12);
        }

        Map<String, Object> content = new HashMap<>();
        content.put("groupHeads", groupHeads);
        content.put("connections", groupsConnections);
        content.put("groupElements", groupsElements);

        return content;
    }



}
