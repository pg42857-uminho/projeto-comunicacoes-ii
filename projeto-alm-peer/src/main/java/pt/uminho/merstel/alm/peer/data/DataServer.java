package pt.uminho.merstel.alm.peer.data;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.utils.Constants;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class DataServer extends Thread {

    private static final Logger logger = LogManager.getLogger(DataServer.class);

    private ThreadPoolExecutor threadPoolExecutor;
    private PeerAgent agent;

    private boolean running = true;

    public DataServer(PeerAgent agent, Configuration configuration) {
        this.agent = agent;
        this.threadPoolExecutor = (ThreadPoolExecutor) Executors
                .newScheduledThreadPool(Constants.DATA_SERVER_THREAD_NUMBER);
    }

    @Override
    public void run() {
        int portNumber = Constants.DATA_PORT_NUMBER;
        int bufferSize = Constants.MAX_BUFFER_SIZE;

        try (DatagramSocket datagramSocket = new DatagramSocket(portNumber)) {
            logger.info("ALM Data Server listening on: " + portNumber);

            while (running) {
                DatagramPacket packet = new DatagramPacket(
                        new byte[bufferSize],
                        bufferSize);

                datagramSocket.receive(packet);

                this.threadPoolExecutor.execute(
                        new DataHandler(
                                packet,
                                agent));
            }
        } catch (IOException e) {
            logger.error("Could not listen on port " + portNumber);
        }
    }

    public void stopRunning() {
        this.running = false;
    }

}
