package pt.uminho.merstel.alm.peer.behaviours;

import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.agent.OverlayManagementAgent;
import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.agent.RPAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.control.ControlClient;
import pt.uminho.merstel.alm.peer.control.ControlHandler;
import pt.uminho.merstel.alm.peer.graph.Edge;
import pt.uminho.merstel.alm.peer.graph.Kruskal;
import pt.uminho.merstel.alm.peer.graph.TreeChart;
import pt.uminho.merstel.alm.peer.utils.ControlMessage;
import pt.uminho.merstel.alm.peer.utils.PDUType;
import pt.uminho.merstel.alm.peer.utils.Pair;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SendTreeAdvertisementsBehaviour extends Behaviour {
    private static final Logger logger = LogManager.getLogger(SendTreeAdvertisementsBehaviour.class);

    private PeerAgent agent;
    private OverlayManagementAgent overlayManagementAgent;
    private Configuration configuration;

    private final List<ControlMessage> peerTimeouts;
    private final List<ControlMessage> peerUpdates;

    private final Object mutex = new Object();

    public SendTreeAdvertisementsBehaviour(PeerAgent agent, RPAgent rpAgent) {
        this.agent = agent;
        this.overlayManagementAgent = rpAgent.getOverlayManagementAgent();
        this.configuration = agent.getConfiguration();

        this.peerTimeouts = new ArrayList<>();
        this.peerUpdates = new ArrayList<>();
    }

    @Override
    public void run() {
        agent.subscribe(PDUType.PEER_TIMEDOUT, null, this);
        agent.subscribe(PDUType.PEER_UPDATE, null, this);

        while(running) {
            if (peerTimeouts.isEmpty() && peerUpdates.isEmpty()) {
                synchronized (mutex) {
                    try {
                        mutex.wait();
                        handleRecentMessages();
                        updateTree();
                    } catch (InterruptedException e) {
                        currentThread().interrupt();
                    }
                }
            }
            else {
                handleRecentMessages();
                updateTree();
            }
        }
    }

    private void handleRecentMessages() {
        List<ControlMessage> timeouts;
        List<ControlMessage> updates;

        synchronized (peerTimeouts) {
            timeouts = new ArrayList<>(peerTimeouts);
            peerTimeouts.clear();
        }

        synchronized (peerUpdates) {
            updates = new ArrayList<>(peerUpdates);
            peerUpdates.clear();
        }

        for (ControlMessage update : updates) {
            Map<String, Object> map = ControlHandler.handlePeerUpdate(update);
            List<Edge> peerConnections = (List<Edge>) map.get("connections");
            overlayManagementAgent.updateConnections(update.source, peerConnections);
        }

        //TIMEOUTS must be after the updates!!!
        for (ControlMessage timeout : timeouts) {
            Map<String, Object> map = ControlHandler.handlePeerTimeout(timeout);
            InetAddress address = (InetAddress) map.get("address");
            overlayManagementAgent.removePeer(address);
        }
    }

    private void updateTree() {
        Map<Integer, List<Edge>> connections = overlayManagementAgent.getAllConnections();
        Map<Integer, List<InetAddress>> peers = overlayManagementAgent.getAllPeers();

        Map<Integer, InetAddress> groupHeads = new HashMap<>();

        logger.debug("------------------------------------");

        logger.debug("PEERS");
        peers.values().forEach(l -> l.forEach(p -> logger.debug(p.getHostAddress())));

        logger.debug("CONNECTIONS");
        connections.values().forEach(l -> l.forEach(e -> logger.debug(e.source.getHostAddress() + " <---> " + e.destination.getHostAddress() + " :: " + e.weight)));

        //RUN KRUSKAL
        Map<Integer, List<Edge>> newTopology = Kruskal.computeWholeTree(
                connections, peers, Configuration.getInstance().getRpAddress(), groupHeads);

        logger.debug("TOPOLOGY RESULTS");
        newTopology.forEach((k, v) -> v.forEach(e -> logger.debug(k + " => " + e.source + " <---> " + e.destination + " :: " + e.weight)));

        logger.debug("GROUP HEADS");
        groupHeads.forEach((k,v) -> logger.debug(v.getHostAddress() + " :: " + k));

        logger.debug("------------------------------------");

        overlayManagementAgent.updateTopology(newTopology, groupHeads);


        TreeChart treeChart = new TreeChart();
        treeChart.generateTreeChart(newTopology);

        ControlClient controlClient = new ControlClient(configuration);
        controlClient.sendTreeAdvertisements(
                overlayManagementAgent.getAllPeersList(), groupHeads, newTopology);
    }

    @Override
    public void handleMessage(ControlMessage message) {
        if (message.pduType == PDUType.PEER_TIMEDOUT) {
            synchronized (mutex) {
                synchronized (peerTimeouts) {
                    peerTimeouts.add(message);
                }
                mutex.notifyAll();
            }
        }
        else if (message.pduType == PDUType.PEER_UPDATE) {
            synchronized (mutex) {
                synchronized (peerUpdates) {
                    peerUpdates.add(message);
                }
                mutex.notifyAll();
            }
        }
    }
}
