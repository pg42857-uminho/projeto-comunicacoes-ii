package pt.uminho.merstel.alm.peer.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.agent.NeighbourManagementAgent;
import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.control.ControlClient;
import pt.uminho.merstel.alm.peer.utils.Constants;
import pt.uminho.merstel.alm.peer.utils.ControlMessage;
import pt.uminho.merstel.alm.peer.utils.NetworkUtils;
import pt.uminho.merstel.alm.peer.utils.PDUType;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ReceiveHelloBehaviour extends Behaviour {

    private static final Logger logger = LogManager.getLogger(ReceiveHelloBehaviour.class);

    private PeerAgent agent;
    private NeighbourManagementAgent neighbourManagementAgent;
    private Configuration configuration;

    private Map<InetAddress, Long> peersHelloTimers = new ConcurrentHashMap<>();

    public ReceiveHelloBehaviour(PeerAgent agent) {
        this.agent = agent;
        this.neighbourManagementAgent = agent.getNeighbourManagementAgent();
        this.configuration = agent.getConfiguration();
    }

    @Override
    public void run() {
        agent.subscribe(PDUType.HELLO, null, this);

        while(running) {
            long now = System.currentTimeMillis();

            List<InetAddress> receiveFrom = new ArrayList<>();

            if (neighbourManagementAgent.isGroupHead()) {
                receiveFrom.addAll(neighbourManagementAgent.getGroupElements());
            }

            if (configuration.getIsRp()) {
                InetAddress localAddress = configuration.getRpAddress();
                receiveFrom.addAll(neighbourManagementAgent.getOtherGroupHeadsList(localAddress));
            }

            receiveFrom.removeIf(NetworkUtils::isLocal);

            List<InetAddress> timedoutAddresses = new ArrayList<>();

            for (InetAddress address: receiveFrom) {
                if (!this.peersHelloTimers.containsKey(address)) {
                    this.peersHelloTimers.put(address, now);
                }
                else {
                    long lastTs = peersHelloTimers.get(address);

                    if (lastTs < (now - Constants.HOLDOUT_TIMER)) {
                        peersHelloTimers.remove(address);
                        neighbourManagementAgent.removePeer(address);

                        timedoutAddresses.add(address);
                    }
                }
            }

            ControlClient controlClient = new ControlClient(configuration);

            for (InetAddress address : timedoutAddresses) {
                controlClient.sendPeerTimeout(address);
            }

            long now1 = System.currentTimeMillis();
            long nextWakeup = getNextWakeUp(now1);

            if (now1 < nextWakeup) {
                try {
                    Thread.sleep(nextWakeup - now1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private synchronized long getNextWakeUp(long now) {
        Optional<Long> minOptional = peersHelloTimers.values()
                .stream()
                .min(Long::compare);

        return minOptional
                .map(aLong -> aLong + Constants.HOLDOUT_TIMER)
                .orElseGet(() -> now + Constants.HOLDOUT_TIMER);
    }

    @Override
    public void handleMessage(ControlMessage message) {
        if (message.pduType == PDUType.HELLO) {
            long timestamp = System.currentTimeMillis();
            peersHelloTimers.put(message.source, timestamp);
        }
    }
}
