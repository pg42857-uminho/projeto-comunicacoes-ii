package pt.uminho.merstel.alm.peer;

import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.configuration.GroupsConfiguration;
import pt.uminho.merstel.alm.peer.control.ControlServer;
import pt.uminho.merstel.alm.peer.data.ALMDataTestClient;
import pt.uminho.merstel.alm.peer.data.DataServer;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length > 1) {
            String arg0 = args[0]; // [ rp | peer ]

            if ("rp".equalsIgnoreCase(arg0)) {
                String groupConfigFile = args[1];

                GroupsConfiguration groupsConfiguration =
                        GroupsConfiguration.loadInstance(groupConfigFile);
                Configuration configuration = Configuration.loadRPConfiguration();

                PeerAgent agent = new PeerAgent(configuration, groupsConfiguration);

                DataServer dataServer = new DataServer(agent, configuration);
                dataServer.start();

                ControlServer controlServer = new ControlServer(agent, configuration);
                controlServer.start();

                ALMDataTestClient almDataTestClient = new ALMDataTestClient(agent);
                almDataTestClient.start();

                agent.loadAgent();
            }
            else if ("peer".equalsIgnoreCase(arg0) && args.length > 1){
                String rpAddress = args[1];

                Configuration configuration = Configuration.loadPeerConfiguration(rpAddress);

                PeerAgent agent = new PeerAgent(configuration, null);

                DataServer dataServer = new DataServer(agent, configuration);
                dataServer.start();

                ControlServer controlServer = new ControlServer(agent, configuration);
                controlServer.start();

                agent.loadAgent();

                if (args.length > 2 && "-s".equalsIgnoreCase(args[2])) {
                    ALMDataTestClient almDataTestClient = new ALMDataTestClient(agent);
                    almDataTestClient.start();
                }
            }
        }
    }
}
