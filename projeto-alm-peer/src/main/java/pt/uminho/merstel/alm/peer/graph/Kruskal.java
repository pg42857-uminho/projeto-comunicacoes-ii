package pt.uminho.merstel.alm.peer.graph;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.utils.Pair;

import java.net.InetAddress;
import java.util.*;

public class Kruskal {

    private static Logger logger = LogManager.getLogger(Kruskal.class);

    private Kruskal() { }

    public static synchronized Map<Integer, List<Edge>> computeWholeTree(
            Map<Integer, List<Edge>> connections,
            Map<Integer, List<InetAddress>> peers,
            InetAddress rpAddress,
            Map<Integer, InetAddress> groupHeads) {

        Map<Integer, List<Edge>> topology = new HashMap<>();

        //CALCULATE THE KRUSKALS FOR EACH GROUP
        /*
        No peers should be in GROUP 0. This group is the central group and only
        GHs should be there!
         */
        for (Map.Entry<Integer, List<InetAddress>> entry : peers.entrySet()) {
            int groupId = entry.getKey();

            List<InetAddress> groupElements = entry.getValue();
            List<Edge> groupConnections = connections.get(groupId);

            //If the group only has one member, that member is the GH
            if (groupElements.size() == 1) {
                groupHeads.put(groupId, groupElements.get(0));
            }
            else if (!groupElements.isEmpty()) {
                //If n > 1 => compute the kruskal!
                List<Edge> groupKruskal = compute(groupConnections, groupElements);
                topology.put(groupId, groupKruskal);

                logger.debug("GROUP :: " + groupId);
                groupKruskal.forEach(e -> logger.debug(e.source + " <-> " + e.destination + " :: " + e.weight));

                //Get the GH and add it to the group heads!
                if (groupElements.contains(rpAddress)) {
                    groupHeads.put(groupId, rpAddress);
                }
                else {
                    InetAddress gh = computeGroupHead(groupKruskal);
                    logger.debug("GROUP " + groupId + " HEAD :: " + gh.getHostAddress());
                    groupHeads.put(groupId, gh);
                }
            }
        }

        List<Edge> groupLevelConnections = new ArrayList<>();

        if (connections.containsKey(0)) {
            groupLevelConnections.addAll(connections.get(0));
        }

        List<Edge> groupLevelTopology = computeGroupHeadLevelTopology(groupHeads, groupLevelConnections);
        topology.put(0, groupLevelTopology);

        groupHeads.put(0, rpAddress);

        return topology;
    }

    private static Map<Integer, Map<Pair<InetAddress, InetAddress>, Integer>> parse(
            Map<Integer, List<Edge>> topology) {

        Map<Integer, Map<Pair<InetAddress, InetAddress>, Integer>> map = new HashMap<>();

        for (Map.Entry<Integer, List<Edge>> topologyEntry : topology.entrySet()) {
            Map<Pair<InetAddress, InetAddress>, Integer> auxMap = new HashMap<>();

            for (Edge edge : topologyEntry.getValue()) {
                auxMap.put(new Pair<>(edge.source, edge.destination), edge.weight);
            }

            map.put(topologyEntry.getKey(), auxMap);
        }

        return map;
    }

    public static List<Edge> computeGroupHeadLevelTopology(
            Map<Integer, InetAddress> groupHeads,
            List<Edge> connections) {

        List<InetAddress> groupHeadsList = new ArrayList<>(groupHeads.values());

        for (int i = 0; i < groupHeadsList.size() - 1; i++) {
            for (int j = i + 1; j < groupHeadsList.size(); j++) {
                InetAddress groupHead0 = groupHeadsList.get(i);
                InetAddress groupHead1 = groupHeadsList.get(j);

                boolean match = connections.stream().anyMatch(e ->
                        (groupHead0.equals(e.source) && groupHead1.equals(e.destination))
                        || (groupHead0.equals(e.destination) && groupHead1.equals(e.source)));

                if (!match) {
                    connections.add(new Edge(groupHead0, groupHead1, Integer.MAX_VALUE));
                }
            }
        }

        /*logger.info("COMPUTE CONNS: " + connections.size() + " GHs: " + groupHeads.size());
        connections.forEach((k, v) -> logger.info(k.getFirst() + " <---> " + k.getSecond() + " :: " + v));
        logger.info("------------------");
        groupHeads.forEach((k, v) -> logger.info(k + " :: " + v.getHostAddress()));
         */

        return compute(connections, groupHeads.values());
    }

    public static InetAddress computeGroupHead(List<Edge> edges) {
        Map<InetAddress, Integer> auxMap = new HashMap<>();

        for (Edge edge : edges) {
            InetAddress peer0 = edge.source;

            if (auxMap.containsKey(peer0)) {
                int value = auxMap.get(peer0);
                auxMap.put(peer0, value+1);
            }
            else {
                auxMap.put(peer0, 1);
            }

            InetAddress peer1 = edge.destination;

            if (auxMap.containsKey(peer1)) {
                int value = auxMap.get(peer1);
                auxMap.put(peer1, value+1);
            }
            else {
                auxMap.put(peer1, 1);
            }
        }

        InetAddress groupHead = null;
        int groupHeadConnNumber = -1;

        for (Map.Entry<InetAddress, Integer> entry : auxMap.entrySet()) {
            if (entry.getValue() > groupHeadConnNumber) {
                groupHead = entry.getKey();
                groupHeadConnNumber = entry.getValue();
            }
            else if (entry.getValue() == groupHeadConnNumber
            && groupHead.getHostAddress().compareTo(entry.getKey().getHostAddress()) < 1) {
                groupHead = entry.getKey();
            }
        }

        return groupHead;
    }

    public static synchronized List<Edge> compute(List<Edge> connections,
                                                  Collection<InetAddress> peers) {

        List<Edge> edges = new ArrayList<>(connections);
        List<InetAddress> vertices = new ArrayList<>(peers);

        return compute0(edges, vertices);
    }

    private static List<Edge> compute0(List<Edge> edges, List<InetAddress> vertices) {
        List<Edge> result = new ArrayList<>();

        LinkedList<List<InetAddress>> disjointSets = new LinkedList<>();

        for (InetAddress vertex : vertices) {
            List<InetAddress> disjointSet = new ArrayList<>();
            disjointSet.add(vertex);
            disjointSets.add(disjointSet);
        }

        LinkedList<Edge> queue = new LinkedList<>(edges);
        Collections.sort(queue);

        boolean stop = false;

        while(!queue.isEmpty() && !stop) {
            Edge edge = queue.pop();

            int dsIndex0 = findSet(edge.source, disjointSets);
            int dsIndex1 = findSet(edge.destination, disjointSets);

            if (dsIndex0 >= 0 && dsIndex1 >= 0 && dsIndex0 != dsIndex1) {
                union(dsIndex0, dsIndex1, disjointSets);
                result.add(edge);
            }
        }

        return result;
    }

    /**
     * Instead of the typical O (logE), it takes O(E) to run.
     * It can be optimized by employing O(logN) search algorithm.
     *
     * @param vertex
     * @param disjointSets
     * @return
     */
    private static int findSet(InetAddress vertex, LinkedList<List<InetAddress>> disjointSets) {
        for (int i = 0; i < disjointSets.size(); i++) {
            List<InetAddress> disjointSet = disjointSets.get(i);

            for (InetAddress otherVertex : disjointSet) {
                if (vertex.equals(otherVertex)) {
                    return i;
                }
            }
        }

        return -1;
    }

    private static void union(int index0, int index1, LinkedList<List<InetAddress>> disjointSets) {
        int lowerIndex = Math.min(index0, index1);
        int upperIndex = Math.max(index0, index1);

        List<InetAddress> upperDisjointSet = disjointSets.remove(upperIndex);
        List<InetAddress> lowerDisjointSet = disjointSets.get(lowerIndex);

        for (InetAddress vertex: upperDisjointSet) {
            lowerDisjointSet.add(vertex);
        }
    }

    public static Map<Pair<InetAddress, InetAddress>, Integer> parseEdgesToMap(List<Edge> edges) {
        Map<Pair<InetAddress, InetAddress>, Integer> connections = new HashMap<>();

        for (Edge edge : edges) {
            InetAddress peer0 = edge.source;
            InetAddress peer1 = edge.destination;
            int metric = edge.weight;

            connections.put(new Pair<>(peer0, peer1), metric);
        }

        return connections;
    }

}
