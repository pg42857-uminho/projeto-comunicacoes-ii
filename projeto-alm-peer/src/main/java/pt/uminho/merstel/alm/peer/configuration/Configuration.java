package pt.uminho.merstel.alm.peer.configuration;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import pt.uminho.merstel.alm.peer.utils.Constants;
import pt.uminho.merstel.alm.peer.utils.NetworkUtils;

import java.io.*;
import java.net.*;
import java.util.Optional;

/**
 * ALM peer configuration class.
 */
public class Configuration {

    private static Configuration instance;

    public static Configuration getInstance() {
        return instance;
    }

    public static Configuration loadRPConfiguration() throws UnknownHostException {
        Optional<InetAddress> optionalLocalAddress = NetworkUtils.getLocalAddress();

        if (optionalLocalAddress.isPresent()) {
            instance = new Configuration(optionalLocalAddress.get().getHostAddress(), true);
        }
        else {
            instance = new Configuration("127.0.0.1", true);
        }

        return instance;
    }

    public static Configuration loadPeerConfiguration(String ipAddress) throws UnknownHostException {
        instance = new Configuration(ipAddress, false);
        return instance;
    }

    // ------------------- INSTANCE ---------------
    private static final String RPADDRESS_PROPERTY = "rp_address";

    private static final String ISRP_PROPERTY = "is_rp";
    private InetAddress rpAddress;

    private boolean isRp;

    private Configuration(String rpAddress, boolean isRp) throws UnknownHostException {
        setRpAddress(rpAddress);
        setIsRp(isRp);
    }

    // ------------------ GETTERS & SETTERS  ----------------------

    public InetAddress getRpAddress() {
        return rpAddress;
    }

    public boolean getIsRp() {
        return isRp;
    }

    private void setRpAddress(String rpAddress) throws UnknownHostException {
        if (rpAddress == null || rpAddress.trim().isEmpty()) {
            throw new IllegalArgumentException("RP Address must not be null, nor empty!");
        }

        if (!Constants.IPADDRESS_PATTERN.matcher(rpAddress).matches()) {
            throw new IllegalArgumentException("RP Address \"" + rpAddress + "\" has an invalid format!");
        }

        this.rpAddress = InetAddress.getByName(rpAddress);
    }

    public void setIsRp(boolean isRp) {
        this.isRp = isRp;
    }

    // -------------------------- Configuration import --------------------------------

    /*public static Configuration createConfiguration() throws URISyntaxException {
        String filename = "peer-configuration.json";

        return importConfiguration(filename);
    }

    private static Configuration importConfiguration(String filename) {
        try {
            InputStream inputStream = Configuration.class.getClassLoader().getResourceAsStream(filename);

            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(new InputStreamReader(inputStream));

            String rpAddress = (String) jsonObject.get(RPADDRESS_PROPERTY);
            //long rpPort = (long) jsonObject.get(RPPORT_PROPERTY);
            //long helloTimer = (long) jsonObject.get(HELLOTIMER_PROPERTY);
            //long controlPort = (long) jsonObject.get(CONTROLPORT_PROPERTY);
            //long dataPort = (long) jsonObject.get(DATAPORT_PROPERTY);
            boolean isRp = (boolean) jsonObject.get(ISRP_PROPERTY);

            long helloTimer = Constants.HELLO_TIMER;
            int controlPort = Constants.CONTROL_PORT_NUMBER;
            int dataPort = Constants.DATA_PORT_NUMBER;
            int rpPort = controlPort;

            Configuration configuration = new Configuration(
                    rpAddress, isRp);
            return configuration;

        } catch (Exception e) {
            throw new RuntimeException("Configuration loading failed!", e);
        }
    }*/


}
