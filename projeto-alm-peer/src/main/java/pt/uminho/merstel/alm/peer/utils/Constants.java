package pt.uminho.merstel.alm.peer.utils;

import java.util.regex.Pattern;

public interface Constants {

    /**
     * IP Address regex pattern.
     * https://www.freeformatter.com/java-regex-tester.html#ad-output
     */
    Pattern IPADDRESS_PATTERN = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");

    Pattern CIDR_PATTERN =
            Pattern.compile("^((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}" +
                    "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?" +
                    "/(?:3[0-2]|[0-2]?[0-9]))" +
                    "|\\*)$");

    int MAX_PORTNUMBER = 65535;
    int CONTROL_SERVER_THREAD_NUMBER = 10;
    int DATA_SERVER_THREAD_NUMBER = 10;

    int AGENTSTATE_DOWN = 0;
    int AGENTSTATE_INIT = 1;
    int AGENTSTATE_UP = 2;

    int CONTROL_PORT_NUMBER = 3000;
    int DATA_PORT_NUMBER = 3001;

    long HELLO_TIMER = 5000;
    long HOLDOUT_TIMER = 20000;

    int RTT_DELTA = 150;
    int RTT_TIMEOUT = 1500;

    int MAX_BUFFER_SIZE = 512;

    String chartFilename = "./plantuml/treeChart.pu";


}
