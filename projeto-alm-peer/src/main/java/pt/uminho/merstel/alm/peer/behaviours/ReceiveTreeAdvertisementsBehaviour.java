package pt.uminho.merstel.alm.peer.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.agent.NeighbourManagementAgent;
import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.control.ControlHandler;
import pt.uminho.merstel.alm.peer.graph.Edge;
import pt.uminho.merstel.alm.peer.utils.ControlMessage;
import pt.uminho.merstel.alm.peer.utils.NetworkUtils;
import pt.uminho.merstel.alm.peer.utils.PDUType;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ReceiveTreeAdvertisementsBehaviour extends Behaviour {

    private static Logger logger = LogManager.getLogger(ReceiveTreeAdvertisementsBehaviour.class);

    private final PeerAgent agent;
    private final NeighbourManagementAgent neighbourManagementAgent;

    //private final Object object = new Object();
    private final LinkedList<ControlMessage> advertisements;

    private final Object mutex = new Object();

    public ReceiveTreeAdvertisementsBehaviour(PeerAgent agent) {
        this.agent = agent;
        this.neighbourManagementAgent = agent.getNeighbourManagementAgent();

        advertisements = new LinkedList<>();
    }

    @Override
    public void run() {
        agent.subscribe(PDUType.TREE_ADV, null, this);

        while (running) {
            if (advertisements.isEmpty()) {
                synchronized (mutex) {
                    try {
                        mutex.wait();
                        handleAdvertisement();
                    } catch (InterruptedException e) {
                        currentThread().interrupt();
                    }
                }
            }
            else {
                handleAdvertisement();
            }
        }
    }

    private void handleAdvertisement() {
        ControlMessage currentMessage;
        synchronized (advertisements) {
            currentMessage = advertisements.getLast();
            advertisements.clear();
        }

        try {
            //Parse
            Map<String, Object> content = ControlHandler.handleTreeAdvertisement(currentMessage);

            //All GHs
            Map<Integer, InetAddress> groupHeads = (Map<Integer, InetAddress>) content.get("groupHeads");
            //Peers divided by group
            Map<Integer, List<InetAddress>> groupElements = (Map<Integer, List<InetAddress>>) content.get("groupElements");
            //All connections
            Map<Integer, List<Edge>> connections = (Map<Integer, List<Edge>>) content.get("connections");

            int groupId = neighbourManagementAgent.getGroupId();

            //SET GROUP HEAD
            InetAddress groupHead = groupHeads.get(groupId);
            neighbourManagementAgent.setGroupHead(groupHead);
            neighbourManagementAgent.setGroupHeads(groupHeads);

            //SET GROUP PEERS
            List<InetAddress> elements = groupElements.get(groupId);
            neighbourManagementAgent.setGroupElements(elements);

            //CALCULATE THE RELEVANT CONNECTIONS (GROUP + GHs if GH)
            List<Edge> relevantConnections = new ArrayList<>(connections.get(groupId));
            if (neighbourManagementAgent.isGroupHead()
                    && connections.containsKey(0)) {
                relevantConnections.addAll(connections.get(0));
            }

            //PARSE CONNECTIONS TO A LIST OF NEIGHBOURS
            List<InetAddress> neighbours = new ArrayList<>();

            for (Edge edge : relevantConnections) {
                boolean a = NetworkUtils.isLocal(edge.source);
                boolean b = NetworkUtils.isLocal(edge.destination);

                if (a) {
                    neighbours.add(edge.destination);
                }
                else if (b) {
                    neighbours.add(edge.source);
                }
            }

            neighbourManagementAgent.setConnections(neighbours);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleMessage(ControlMessage message) {
        if (message.pduType == PDUType.TREE_ADV) {
            synchronized (mutex) {
                synchronized (advertisements) {
                    advertisements.addLast(message);
                }

                mutex.notifyAll();
            }
        }
    }
}
