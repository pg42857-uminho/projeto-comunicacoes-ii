package pt.uminho.merstel.alm.peer.utils;

public interface PDUType {

    byte HELLO = 0x01;

    byte JOIN_REQ = 0x02;
    byte JOIN_ACK = 0x03;

    byte PEER_UPDATE = 0x04;
    byte PEER_TIMEDOUT = 0x05;

    byte TREE_ADV = 0x08;

}
