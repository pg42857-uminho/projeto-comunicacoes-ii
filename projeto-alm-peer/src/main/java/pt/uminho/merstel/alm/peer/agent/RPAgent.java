package pt.uminho.merstel.alm.peer.agent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.configuration.GroupsConfiguration;
import pt.uminho.merstel.alm.peer.utils.ControlMessage;

import java.util.*;

public class RPAgent {

    private Logger logger = LogManager.getLogger(RPAgent.class);

    private OverlayManagementAgent overlayManagementAgent;

    public RPAgent(Configuration configuration, GroupsConfiguration groupsConfiguration) {
        this.overlayManagementAgent = new OverlayManagementAgent(groupsConfiguration);
    }

    public OverlayManagementAgent getOverlayManagementAgent() {
        return this.overlayManagementAgent;
    }

    // --------------------------- OTHER METHODS ---------------------------

    /*public synchronized void registerNewPeer(InetAddress peerAddress) {
        int peerId = createNewPeer(peerAddress);

        sendJoinAcknowledgement(peerId);

        schedulePeerTest(peerId, peerAddress);
        scheduleTreeUpdate();
    }

    private synchronized void schedulePeerTest(int peerId, InetAddress peerAddress) {
        Timer timer0 = new Timer();
        timer0.schedule(new TimerTask() {
            @Override
            public void run() {
                sendTestPeerRequest(peerId);
                sendRTTTestRequest(peerId, peerAddress);
            }
        }, NEW_PEER_TEST_BEGINNING_INTERVAL);
    }*/

    /*private synchronized void scheduleTreeUpdate() {
        Timer timer1 = new Timer();
        timer1.schedule(new TimerTask() {
            @Override
            public void run() {
                treeUpdate();
            }
        }, NEW_PEER_TEST_INTERVAL);
    }*/

    /*private synchronized void treeUpdate() {
        Map<Integer, Map<Integer, Integer>> connectionMap = super.getConnections();
        logger.info("ALM RP Connection Map Size: " + connectionMap.size());

        if (!connectionMap.isEmpty()) {
            List<Integer> peers = super.getPeerIds();

//            List<Edge> edges = Kruskal.compute(connectionMap, peers);

            for (Edge edge : edges) {
                logger.info("Edge :: " + edge.getSource() +
                        " <-> " + edge.getDestination() + " : " + edge.getWeight());
            }

     //       setNeighbors(Kruskal.parseEdgesToMap(edges));

  //          sendTreeAdvertisements(edges);
        }
    }*/

    /*@Override
    public synchronized void registerRTT(int peerId, long timestamp) {
        super.registerRTT(peerId, timestamp);
        addConnection(super.getPeerId(), peerId, (int) timestamp);
    }

    public synchronized void registerHello(int peerId, long timestamp) {
        helloTracker.registerHello(peerId, timestamp);
    }

    public synchronized void removePeer(List<InetAddress> timedOutPeers) {
        super.removePeer(peerList);
        if (!peerList.isEmpty()) {
            scheduleTreeUpdate();
        }
    }*/

}
