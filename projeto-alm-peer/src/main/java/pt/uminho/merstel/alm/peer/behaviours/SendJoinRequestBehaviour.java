package pt.uminho.merstel.alm.peer.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.agent.NeighbourManagementAgent;
import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.control.ControlClient;
import pt.uminho.merstel.alm.peer.control.ControlHandler;
import pt.uminho.merstel.alm.peer.utils.ControlMessage;
import pt.uminho.merstel.alm.peer.utils.PDUType;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

public class SendJoinRequestBehaviour extends Behaviour {

    private static Logger logger = LogManager.getLogger(Behaviour.class);

    private PeerAgent agent;
    private Configuration configuration;

    private int state;

    private ControlMessage joinAck;

    public SendJoinRequestBehaviour(PeerAgent agent) {
        this.agent = agent;
        this.configuration = agent.getConfiguration();
    }

    @Override
    public void run() {
        while (running) {
            switch (state) {
                case 0:
                    InetAddress inetAddress = configuration.getRpAddress();
                    agent.subscribe(PDUType.JOIN_ACK, inetAddress, this);

                    ControlClient controlClient = new ControlClient(
                            agent.getConfiguration());
                    controlClient.sendJoinRequest();
                    state = 1;
                    break;

                case 1:
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (joinAck != null) {
                        state = 2;
                    }

                    break;

                case 2:
                    if (joinAck != null) {
                        handleJoinAck();
                    }

                    agent.unsubscribe(this);
                    super.stopRunning();
                    break;

                default:
                    logger.error("Should not happen!");
                    break;
            }
        }
    }

    private void handleJoinAck() {
        try {
            Map<String, Object> map = ControlHandler.handleJoinAcknowledgement(joinAck);

            NeighbourManagementAgent neighbourManagementAgent =
                    agent.getNeighbourManagementAgent();

            int groupId = (int) map.get("groupId");
            InetAddress groupHead = (InetAddress) map.get("groupHead");

            neighbourManagementAgent.setGroupId(groupId);
            neighbourManagementAgent.setGroupHead(groupHead);

            List<InetAddress> groupElements = (List<InetAddress>) map.get("groupElements");
            Map<Integer, InetAddress> groupHeads = (Map<Integer, InetAddress>) map.get("groupHeads");

            neighbourManagementAgent.setGroupElements(groupElements);
            neighbourManagementAgent.setGroupHeads(groupHeads);

            logger.info("Peer registered on  group " + groupId + " with GH " +
                    groupHead.getHostAddress());

            ReceiveTreeAdvertisementsBehaviour receiveTreeAdvertisementsBehaviour
                    = new ReceiveTreeAdvertisementsBehaviour(agent);
            agent.addBehaviour(receiveTreeAdvertisementsBehaviour);

            SendPeerUpdatesBehaviour sendPeerUpdatesBehaviour
                    = new SendPeerUpdatesBehaviour(agent);
            agent.addBehaviour(sendPeerUpdatesBehaviour);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleMessage(ControlMessage message) {
        if (message.pduType == PDUType.JOIN_ACK) {
            this.joinAck = message;
        }
    }
}
