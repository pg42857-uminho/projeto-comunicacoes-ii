package pt.uminho.merstel.alm.peer.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.graph.Edge;
import pt.uminho.merstel.alm.peer.graph.TreeChart;
import pt.uminho.merstel.alm.peer.utils.*;

import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ControlClient {

    private static Logger logger = LogManager.getLogger(ControlClient.class);

    private Configuration configuration;
    private TreeChart treeChart;

    public ControlClient(Configuration configuration) {
        this.configuration = configuration;
        this.treeChart = new TreeChart();
    }

    public void sendHello(List<InetAddress> addresses) {
        byte[] array = new byte[4];
        array[0] = PDUType.HELLO;

        SendMessage instance = SendMessage.getInstance();

        for (InetAddress address : addresses) {
            logger.info("Peer sent HELLO to RP at " + address.getHostAddress());

            instance.sendMessageTCP(address, Constants.CONTROL_PORT_NUMBER, array);
        }
    }

    public void sendJoinRequest() {
        byte[] array = new byte[4];
        array[0] = PDUType.JOIN_REQ;
        InetAddress address = configuration.getRpAddress();

        logger.info("Peer sent JOIN_REQ to RP at: " + address.getHostAddress());

        SendMessage.getInstance().sendMessageTCP(address, Constants.CONTROL_PORT_NUMBER, array);
    }

    public void sendJoinAcknowledgement(InetAddress ipAddress,
                                        int groupId,
                                        InetAddress groupHead,
                                        List<InetAddress> groupElems,
                                        Map<Integer, InetAddress> groupHeads) {

        logger.info("Group ID : " + groupId);
        logger.info("GH Address : " + groupHead.getHostAddress());

        logger.info("GH Elements");
        groupElems.forEach(o -> logger.info(o.getHostAddress()));

        logger.info("GHs");
        groupElems.forEach(o -> logger.info(o.getHostAddress()));

        List<InetAddress> groupAddresses = new ArrayList<>();
        groupAddresses.add(groupHead);
        groupAddresses.addAll(groupElems);

        int length = 4 + 4 * groupAddresses.size() + 8 * groupHeads.size();

        byte[] array = new byte[length];
        array[0] = PDUType.JOIN_ACK;
        array[1] = (byte) groupId;
        array[2] = (byte) groupElems.size();
        array[3] = (byte) groupHeads.size();

        for (int i = 0; i < groupAddresses.size(); i++) {
            byte[] addressBytes = groupAddresses.get(i).getAddress();

            for (int j = 0; j < addressBytes.length; j++) {
                array[4 + (i*4) + j] = addressBytes[j];
            }
        }

        int lastIndex = 0;
        for (Map.Entry<Integer, InetAddress> entry : groupHeads.entrySet()) {
            InetAddress groupHeadAddress = entry.getValue();
            int groupHeadId = entry.getKey();

            byte[] ghAddressBytes = groupHeadAddress.getAddress();
            byte[] ghIdBytes = DataUtils.intToBytes(groupHeadId);

            for (int i = 0; i < 4; i++) {
                array[4 + 4 * groupAddresses.size() + 8 * lastIndex + i] = ghIdBytes[i];
                array[4 + 4 * groupAddresses.size() + 8 * lastIndex + 4 + i] = ghAddressBytes[i];
            }

            lastIndex++;
        }


        //logger.info("RP sent JOIN_ACK to: " + ipAddress.getHostAddress());
        //logger.info("GroupId : " + groupId);
        //allAddresses.forEach(o -> logger.info(o.getHostAddress()));

        SendMessage.getInstance().sendMessageTCP(
                ipAddress,
                Constants.CONTROL_PORT_NUMBER,
                array);
    }

    public Map<InetAddress, Integer> testRTT(List<InetAddress> addresses) {
        Map<InetAddress, Integer> map = new ConcurrentHashMap<>();
        Thread[] threads = new Thread[addresses.size()];

        for (int i = 0; i < addresses.size(); i++) {
            InetAddress address = addresses.get(i);

            Thread thread = new Thread(() -> {
                long ts0 = System.currentTimeMillis();
                long ts1 = Long.MAX_VALUE;

                try {
                    address.isReachable(Constants.RTT_TIMEOUT);
                    ts1 = System.currentTimeMillis();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                int metric = (int) (ts1 - ts0);
                map.put(address, metric);
            });

            threads[i] = thread;
            thread.start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                thread.interrupt();
                e.printStackTrace();
            }
        }

        logger.info("Peer tested RTT!");

        return map;
    }

    public void sendPeerUpdate(Map<InetAddress, Integer> map) {
        int length = 4 + (map.size() * 8);

        byte[] buffer = new byte[length];
        buffer[0] = PDUType.PEER_UPDATE;
        buffer[1] = (byte) map.size();

        int lastIndex = 4;

        for (Map.Entry<InetAddress, Integer> entry : map.entrySet()) {
            InetAddress address = entry.getKey();
            int metric = entry.getValue();

            byte[] addressBytes = address.getAddress();
            byte[] metricBytes = DataUtils.intToBytes(metric);

            for (int i = 0; i < 4; i++) {
                buffer[lastIndex + i] = addressBytes[i];
                buffer[lastIndex + 4 + i] = metricBytes[i];
            }

            lastIndex += 8;
        }

        InetAddress address = configuration.getRpAddress();

        logger.info("Peer sent PEER UPDATE to RP at " + address.getHostAddress());

        /*for (Map.Entry<InetAddress, Integer> entry : map.entrySet()) {
            logger.info(entry.getKey().getHostAddress() + " :: " + entry.getValue());
        }*/

        SendMessage.getInstance().sendMessageTCP(address, Constants.CONTROL_PORT_NUMBER, buffer);
    }

    public void sendPeerUpdate0(int groupId,
                               InetAddress groupHeadAddress,
                               List<InetAddress> groupElem,
                               List<InetAddress> headElem,
                               List<Integer> groupMetrics,
                               List<Integer> headMetrics) {

        byte groupIdByte = (byte) groupId;
        byte nGroupElemByte = (byte) ((byte) groupElem.size() * 4);
        byte nHeadElemByte = (byte) ((byte) headElem.size() * 4);

        byte[] groupHeadAddressBytes = groupHeadAddress.getAddress();

        List<InetAddress> allAddresses = new ArrayList<>();
        allAddresses.addAll(groupElem);
        allAddresses.addAll(headElem);

        List<Integer> metrics = new ArrayList<>();
        metrics.addAll(groupMetrics);
        metrics.addAll(headMetrics);

        int length = 8 + nGroupElemByte + nHeadElemByte;

        byte[] buffer = new byte[length];
        buffer[0] = PDUType.PEER_UPDATE;
        buffer[1] = groupIdByte;
        buffer[2] = nGroupElemByte;
        buffer[3] = nHeadElemByte;

        for (int i = 0; i < 4; i++) {
            buffer[4+i] = groupHeadAddressBytes[i];
        }

        for (int i = 0; i < allAddresses.size(); i++) {
            byte[] addressBytes = allAddresses.get(i).getAddress();
            byte[] metric = DataUtils.intToBytes(metrics.get(i));

            for (int j = 0; j < 4; j++) {
                buffer[8 + (i * 8) + j] = addressBytes[i];
                buffer[8 + (i * 8) + j + 4] = metric[i];
            }
        }

        InetAddress address = configuration.getRpAddress();

        logger.info("Peer sent HELLO to RP at " + address.getHostAddress());

        SendMessage.getInstance().sendMessageTCP(address,
                Constants.CONTROL_PORT_NUMBER, buffer);
    }

    public void sendPeerTimeout(InetAddress address) {
        byte[] buffer = new byte[8];
        buffer[0] = PDUType.PEER_TIMEDOUT;

        byte[] addressBytes = address.getAddress();

        for (int i = 0; i < 4; i++) {
            buffer[4 + i] = addressBytes[i];
        }

        logger.info("RP sent PEER_TIMEOUT for " + address.getHostAddress());
        InetAddress destinationAddress = configuration.getRpAddress();

        SendMessage.getInstance().sendMessageTCP(destinationAddress, Constants.CONTROL_PORT_NUMBER, buffer);
    }

    public void sendTreeAdvertisements(Collection<InetAddress> addresses,
                                       Map<Integer, InetAddress> groupHeads,
                                       Map<Integer, List<Edge>> connections) {
        byte numberOfGroups = (byte) groupHeads.size();

        int length = 4 + numberOfGroups * 8;

        for (List<Edge> entry : connections.values()) {
            length += entry.size() * 12;
        }

        byte[] buffer = new byte[length];
        buffer[0] = PDUType.TREE_ADV;
        buffer[1] = numberOfGroups;

        int groupIndex = 4;


        //IT DOESNT PROCESS GROUP 0 : NO GROUP HEADS IN GROUP 0
        //IT FAILS IF THE GROUP HAS NO CONNECTIONS: ONLY 1 ELEM
        for (Map.Entry<Integer, InetAddress> entry : groupHeads.entrySet()) {
            int groupId = entry.getKey();

            List<Edge> groupConnections = new ArrayList<>();
            if (connections.containsKey(groupId)) {
                groupConnections.addAll(connections.get(groupId));
            }

            buffer[groupIndex] = (byte) groupId;
            buffer[groupIndex+1] = (byte) groupConnections.size();

            byte[] ghAddress = entry.getValue().getAddress();
            for (int i = 0; i < 4; i++) {
                buffer[groupIndex + 4 + i] = ghAddress[i];
            }

            int i = 0;
            for (Edge connection : groupConnections) {
                int connectionIndex = groupIndex + 8 + (i * 12);

                InetAddress address1 = connection.source;
                byte[] address1Bytes = address1.getAddress();

                for (int j = 0; j < 4; j++) {
                    buffer[connectionIndex + j] = address1Bytes[j];
                }

                InetAddress address2 = connection.destination;
                byte[] address2Bytes = address2.getAddress();

                for (int j = 0; j < 4; j++) {
                    buffer[connectionIndex + 4 + j] = address2Bytes[j];
                }

                int metric = connection.weight;
                byte[] metricBytes = DataUtils.intToBytes(metric);

                for (int j = 0; j < 4; j++) {
                    buffer[connectionIndex + 8 + j] = metricBytes[j];
                }

                i++;
            }

            groupIndex += 8 + (groupConnections.size() * 12);
        }

        SendMessage instance = SendMessage.getInstance();

        for (InetAddress address : addresses) {
            instance.sendMessageTCP(address,
                    Constants.CONTROL_PORT_NUMBER, buffer);
        }

        logger.info("Peer sent TREE ADVs");
    }

}
