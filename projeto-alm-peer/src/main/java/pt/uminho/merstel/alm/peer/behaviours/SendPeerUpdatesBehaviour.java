package pt.uminho.merstel.alm.peer.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.agent.NeighbourManagementAgent;
import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.control.ControlClient;
import pt.uminho.merstel.alm.peer.utils.Constants;
import pt.uminho.merstel.alm.peer.utils.ControlMessage;
import pt.uminho.merstel.alm.peer.utils.NetworkUtils;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SendPeerUpdatesBehaviour extends Behaviour {

    private Logger logger = LogManager.getLogger(SendPeerUpdatesBehaviour.class);

    private PeerAgent agent;
    private NeighbourManagementAgent neighbourManagementAgent;
    private Configuration configuration;

    private Map<InetAddress, Integer> lastRtts;

    public SendPeerUpdatesBehaviour(PeerAgent agent) {
        this.agent = agent;
        this.configuration = agent.getConfiguration();
        this.neighbourManagementAgent = agent.getNeighbourManagementAgent();
        this.lastRtts = new ConcurrentHashMap<>();
    }

    @Override
    public void run() {
        while(running) {
            List<InetAddress> peersToTest = new ArrayList<>(
                    neighbourManagementAgent.getOtherGroupElements());

            if (neighbourManagementAgent.isGroupHead()) {
                peersToTest.addAll(neighbourManagementAgent.getOtherGroupHeads().values());
            }
            else {
                lastRtts.keySet().removeIf(a -> !a.equals(neighbourManagementAgent.getGroupHead()));
            }

            logger.info("----------------------------");
            logger.info("TO TEST");
            peersToTest.forEach(a -> logger.info(a.getHostAddress()));

            logger.info("%nLAST RTTS");
            lastRtts.keySet().forEach(a -> logger.info(a.getHostAddress()));
            logger.info("-------------------------------");

            lastRtts = updateResults(peersToTest);

            ControlClient controlClient = new ControlClient(configuration);
            Map<InetAddress, Integer> rtts = controlClient.testRTT(peersToTest);

            if (hasChanged(lastRtts, rtts)) {
                lastRtts = treatResults(lastRtts, rtts);

                for (Map.Entry<InetAddress, Integer> entry : lastRtts.entrySet()) {
                    logger.info(entry.getKey().getHostAddress() + " :: " + entry.getValue());
                }

                controlClient.sendPeerUpdate(lastRtts);
            }

            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                currentThread().interrupt();
                e.printStackTrace();
            }
        }
    }

    private Map<InetAddress, Integer> updateResults(List<InetAddress> addresses) {
        // Remove all entries that have been removed
        lastRtts.keySet().removeIf(a -> !addresses.contains(a));

        for (InetAddress address: addresses) {
            lastRtts.putIfAbsent(address, Integer.MAX_VALUE);
        }

        return lastRtts;
    }

    private static Map<InetAddress, Integer> treatResults(Map<InetAddress, Integer> lastRtts,
                                                          Map<InetAddress, Integer> rtts) {

        Map<InetAddress, Integer> newResults = new HashMap<>(rtts);

        for (Map.Entry<InetAddress, Integer> entry : rtts.entrySet()) {
            if (lastRtts.containsKey(entry.getKey())) {
                int metric0 = entry.getValue();
                int metric1 = lastRtts.get(entry.getKey());

                if (Math.abs(metric0 - metric1) < Constants.RTT_DELTA) {
                    newResults.put(entry.getKey(), metric1);
                }
                else {
                    newResults.put(entry.getKey(), metric0);
                }
            }
            else {
                newResults.put(entry.getKey(), entry.getValue());
            }
        }

        return newResults;
    }

    private static boolean hasChanged(Map<InetAddress, Integer> lastRtts,
                                      Map<InetAddress, Integer> rtts) {

        for (Map.Entry<InetAddress, Integer> entry : rtts.entrySet()) {
            InetAddress address = entry.getKey();

            int metric = entry.getValue();
            int lastMetric = lastRtts.get(address);

            if (Math.abs(metric - lastMetric) > Constants.RTT_DELTA) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void handleMessage(ControlMessage message) {
        //DO NOTHING
    }
}
