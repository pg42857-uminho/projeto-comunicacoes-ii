package pt.uminho.merstel.alm.peer.behaviours;

import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.utils.ControlMessage;

public abstract class Behaviour extends Thread {

    public boolean running = false;

    @Override
    public void start() {
        this.running = true;
        super.start();
    }

    public abstract void run();
    public abstract void handleMessage(ControlMessage message);

    public void stopRunning() {
        this.running = false;
    }

}
