package pt.uminho.merstel.alm.peer.graph;

import java.net.InetAddress;

public class Edge implements Comparable<Edge> {

    public InetAddress source;
    public InetAddress destination;
    public int weight;

    public Edge(InetAddress source, InetAddress destination, int weight) {
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    @Override
    public int compareTo(Edge o) {
        return Integer.compare(this.weight, o.weight);
    }

}
