package pt.uminho.merstel.alm.peer.agent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.behaviours.*;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.configuration.GroupsConfiguration;
import pt.uminho.merstel.alm.peer.utils.ControlMessage;
import pt.uminho.merstel.alm.peer.utils.Pair;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class PeerAgent {

    private static Logger logger = LogManager.getLogger(PeerAgent.class);

    private final Configuration configuration;

    private final RPAgent rpAgent;
    private final NeighbourManagementAgent neighbourManagementAgent;

    private final List<Behaviour> behaviours = new ArrayList<>();
    private final Map<Pair<Byte, InetAddress>, Behaviour> subscriptions = new ConcurrentHashMap<>();

    public PeerAgent(Configuration configuration, GroupsConfiguration groupsConfiguration) {
        this.configuration = configuration;
        this.neighbourManagementAgent = new NeighbourManagementAgent(this);

        if (configuration.getIsRp()) {
            this.rpAgent = new RPAgent(configuration, groupsConfiguration);
            InetAddress localAddress = configuration.getRpAddress();

            OverlayManagementAgent overlayManagementAgent = rpAgent.getOverlayManagementAgent();
            overlayManagementAgent.addPeer(localAddress);
            overlayManagementAgent.addGroupHead(localAddress);

            int groupId = groupsConfiguration.getGroup(localAddress);
            neighbourManagementAgent.setGroupId(groupId);
            neighbourManagementAgent.setGroupHead(localAddress);
        }
        else {
            this.rpAgent = null;
        }
    }

    public void loadAgent() {
        ReceiveHelloBehaviour receiveHelloBehaviour = new ReceiveHelloBehaviour(this);
        addBehaviour(receiveHelloBehaviour);

        if (configuration.getIsRp()) {
            ReceiveTreeAdvertisementsBehaviour receiveTreeAdvertisementsBehaviour
                    = new ReceiveTreeAdvertisementsBehaviour(this);
            addBehaviour(receiveTreeAdvertisementsBehaviour);

            ReceiveJoinRequestBehaviour receiveJoinRequestBehaviour
                    = new ReceiveJoinRequestBehaviour(this, rpAgent);
            addBehaviour(receiveJoinRequestBehaviour);

            SendTreeAdvertisementsBehaviour sendTreeAdvertisementsBehaviour
                    = new SendTreeAdvertisementsBehaviour(this, rpAgent);
            addBehaviour(sendTreeAdvertisementsBehaviour);
        }
        else {
            SendHelloBehaviour sendHelloBehaviour = new SendHelloBehaviour(this);
            addBehaviour(sendHelloBehaviour);

            SendJoinRequestBehaviour sendJoinRequestBehaviour
                    = new SendJoinRequestBehaviour(this);
            addBehaviour(sendJoinRequestBehaviour);
        }
    }

    public void addBehaviour(Behaviour behaviour) {
        this.behaviours.add(behaviour);
        behaviour.start();
    }

    public NeighbourManagementAgent getNeighbourManagementAgent() {
        return neighbourManagementAgent;
    }

    // ------------------------ BEHAVIOURS ----------------------------

    public void subscribe(byte pduType, InetAddress fromAddress, Behaviour behaviour) {
        Pair<Byte, InetAddress> pair = new Pair<>(pduType, fromAddress);
        this.subscriptions.put(pair, behaviour);
    }

    public void unsubscribe(byte pduType, InetAddress fromAddress) {
        Pair<Byte, InetAddress> pair = new Pair<>(pduType, fromAddress);
        if (this.subscriptions.containsKey(pair)) {
            this.subscriptions.remove(pair);
            //TODO: If more than one behaviour is subscribed to a pdu and inetaddress,
            // a list may be used instead of a single behaviour
        }
    }

    public synchronized void unsubscribe(final Behaviour behaviour) {
        this.subscriptions.entrySet().removeIf(
                entry -> entry.getValue().equals(behaviour));
    }

    public synchronized void publish(ControlMessage controlMessage) {
        for (Map.Entry<Pair<Byte, InetAddress>, Behaviour> subscription : subscriptions.entrySet()) {
            Pair<Byte, InetAddress> subscriptionKey = subscription.getKey();

            // If PDU corresponds AND (address == null || address corresponds)
            if(subscriptionKey.getFirst().equals(controlMessage.pduType) &&
                    (subscriptionKey.getSecond() == null
                            || subscriptionKey.getSecond().equals(controlMessage.source))) {

                Behaviour behaviour = subscription.getValue();
                behaviour.handleMessage(controlMessage);
//                behaviour.notify();
            }
        }
    }

    // ------------------------- HANDLE ----------------------

    public void handleMessage(ControlMessage message) {
        publish(message);
    }

    // -------------------------- OBJECT METHODS ---------------------------

    public synchronized Configuration getConfiguration() {
        return this.configuration;
    }



    // -------------------- CONNECTIONS METHODS --------------------------

    /*public synchronized void setNeighbors(Map<Integer, Map<Integer, Integer>> multicastTree) {
        synchronized (neighbors) {
            neighbors.clear();

            for (Map.Entry<Integer, Map<Integer, Integer>> index0 : multicastTree.entrySet()) {
                int peerId0 = index0.getKey();

                for (Map.Entry<Integer, Integer> index1 : index0.getValue().entrySet()) {
                    int peerId1 = index1.getKey();

                    if (this.peerId == peerId0) {
                        InetAddress peerAddress = getPeerAddress(peerId1);
                        neighbors.put(peerId1, peerAddress);
                    } else if (this.peerId == peerId1) {
                        InetAddress peerAddress = getPeerAddress(peerId0);
                        neighbors.put(peerId0, peerAddress);
                    }
                }
            }

            for (InetAddress inetAddress : getNeighbourAddresses()) {
                logger.info("ALM Neighbour: " + inetAddress.getHostAddress());
            }
        }
    }*/

    /*public synchronized Collection<InetAddress> getNeighbourAddresses() {
        return this.neighbors.values();
    }

    public synchronized void addConnection(int peerId, int otherPeerId, int metric) {
        if (this.connections.containsKey(peerId)) {
            Map<Integer, Integer> map = this.connections.get(peerId);
            map.put(otherPeerId, metric);
        } else {
            Map<Integer, Integer> map = new HashMap<>();
            map.put(otherPeerId, metric);
            this.connections.put(peerId, map);
        }
    }

    public synchronized void removePeer(List<Integer> peerList) {
        peers.keySet().removeAll(peerList);

        synchronized (connections) {
            connections.keySet().removeAll(peerList);
            connections.values().forEach(map -> map.values().removeAll(peerList));
        }
    }

    protected synchronized Map<Integer, Map<Integer, Integer>> getConnections() {
        return new HashMap<>(this.connections);
    }

    protected synchronized List<Integer> getPeerIds() {
        return new ArrayList<>(peers.keySet());
    }*/

}
