package pt.uminho.merstel.alm.peer.behaviours;

import pt.uminho.merstel.alm.peer.agent.NeighbourManagementAgent;
import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.configuration.Configuration;
import pt.uminho.merstel.alm.peer.control.ControlClient;
import pt.uminho.merstel.alm.peer.utils.Constants;
import pt.uminho.merstel.alm.peer.utils.ControlMessage;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class SendHelloBehaviour extends Behaviour {

    private PeerAgent agent;
    private NeighbourManagementAgent neighbourManagementAgent;
    private Configuration configuration;

    public SendHelloBehaviour(PeerAgent agent) {
        this.agent = agent;
        this.neighbourManagementAgent = agent.getNeighbourManagementAgent();
        this.configuration = agent.getConfiguration();
    }

    @Override
    public void run() {
        while (true) {
            List<InetAddress> peersToHello = new ArrayList<>();

            if (neighbourManagementAgent.isGroupHead()) {
                peersToHello.add(configuration.getRpAddress());
            }
            else {
                peersToHello.add(neighbourManagementAgent.getGroupHead());
            }

            ControlClient controlClient = new ControlClient(agent.getConfiguration());
            controlClient.sendHello(peersToHello);

            try {
                Thread.sleep(Constants.HELLO_TIMER);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleMessage(ControlMessage message) {
        // Will never handle a message
    }

}
