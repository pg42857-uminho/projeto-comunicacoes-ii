package pt.uminho.merstel.alm.peer.behaviours;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.agent.OverlayManagementAgent;
import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.agent.RPAgent;
import pt.uminho.merstel.alm.peer.configuration.GroupsConfiguration;
import pt.uminho.merstel.alm.peer.control.ControlClient;
import pt.uminho.merstel.alm.peer.utils.ControlMessage;
import pt.uminho.merstel.alm.peer.utils.PDUType;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ReceiveJoinRequestBehaviour extends Behaviour {

    private static Logger logger = LogManager.getLogger(ReceiveJoinRequestBehaviour.class);

    private PeerAgent agent;
    private GroupsConfiguration groupsConfiguration;
    private OverlayManagementAgent overlayManagementAgent;

    private final List<ControlMessage> joinRequests = new ArrayList<>();
    private final Object mutex = new Object();

    public ReceiveJoinRequestBehaviour(PeerAgent agent, RPAgent rpAgent) {
        this.agent = agent;
        this.overlayManagementAgent = rpAgent.getOverlayManagementAgent();
        this.groupsConfiguration = GroupsConfiguration.getInstance();
    }

    @Override
    public void run() {
        agent.subscribe(PDUType.JOIN_REQ, null, this);

        while(running) {
            if (joinRequests.isEmpty()) {
                synchronized (mutex) {
                    try {
                        mutex.wait();
                        handleJoinRequests();
                    } catch (InterruptedException e) {
                        currentThread().interrupt();
                        e.printStackTrace();
                    }
                }
            }
            else {
                handleJoinRequests();
            }
        }
    }

    private void handleJoinRequests() {
        List<ControlMessage> messages;

        synchronized (joinRequests) {
            messages = new ArrayList<>(joinRequests);
            joinRequests.clear();
        }

        for (ControlMessage message : messages) {
            handleJoinRequest(message);
        }
    }

    private void handleJoinRequest(ControlMessage message) {
        InetAddress address = message.source;
        int groupId = groupsConfiguration.getGroup(address);
        overlayManagementAgent.addPeer(address, groupId);

        List<InetAddress> groupPeers = overlayManagementAgent.getPeers(groupId);
        Map<Integer, InetAddress> groupHeads = overlayManagementAgent.getGroupHeads();
        InetAddress groupHead = overlayManagementAgent.getGroupHead(groupId);


        Thread thread = new Thread(() -> {
            ControlClient controlClient = new ControlClient(agent.getConfiguration());
            controlClient.sendJoinAcknowledgement(
                    address, groupId, groupHead, groupPeers, groupHeads);
        });
        thread.start();
    }

    @Override
    public void handleMessage(ControlMessage message) {
        if (message.pduType == PDUType.JOIN_REQ) {
            synchronized (mutex) {
                synchronized (joinRequests) {
                    joinRequests.add(message);
                }
                mutex.notifyAll();
            }
        }
    }
}
