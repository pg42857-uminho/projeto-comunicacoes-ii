package pt.uminho.merstel.alm.peer.data;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.agent.NeighbourManagementAgent;
import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.utils.Constants;
import pt.uminho.merstel.alm.peer.utils.SendMessage;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;

public class DataHandler implements Runnable {

    private static final Logger logger = LogManager.getLogger(DataHandler.class);

    //private final Collection<InetAddress> neighborAddresses;
    private final DatagramPacket packet;
    private final PeerAgent agent;
    private final NeighbourManagementAgent neighbourManagementAgent;

    public DataHandler(final DatagramPacket packet, final PeerAgent agent) {
        this.packet = packet;
        this.agent = agent;
        this.neighbourManagementAgent = agent.getNeighbourManagementAgent();
    }

    @Override
    public void run() {
        byte[] array = packet.getData();
        InetAddress peerAddress = packet.getAddress();

        //logger.info("Received message: " + new String(array));

        System.out.println(new String(array));

        for (InetAddress inetAddress : neighbourManagementAgent.getOtherConnections(peerAddress)) {
            //Split horizon
            SendMessage.getInstance().sendMessageUDP(inetAddress, Constants.DATA_PORT_NUMBER, array);
        }
    }

}
