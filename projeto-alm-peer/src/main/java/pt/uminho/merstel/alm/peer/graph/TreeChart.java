package pt.uminho.merstel.alm.peer.graph;

import pt.uminho.merstel.alm.peer.utils.Constants;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TreeChart {
    public void generateTreeChart(Map<Integer, List<Edge>> all){
        final List<Edge> edgeList = new ArrayList<>();
        all.values().forEach(edgeList::addAll);

        String theComponentContent = "@startuml\nskinparam monochrome true\n\n";
        List<String> theNodeList = new ArrayList<>();
        for(Edge edge : edgeList){
            if(!theNodeList.contains(edge.source.getHostAddress())){
                theNodeList.add(edge.source.getHostName());
            }if (!theNodeList.contains(edge.destination.getHostAddress())){
                theNodeList.add(edge.destination.getHostName());
            }
        }
        for(String node : theNodeList){
            theComponentContent = theComponentContent+"[Node "+node+"] as node"+node+"\n";
        }
        for (Edge edge : edgeList){
            theComponentContent = theComponentContent+"node"+edge.source.getHostAddress()
                    +" <--> "+"node"+edge.destination.getHostAddress()
                    +" : Weight "+edge.weight+"\n";
        }
        theComponentContent = theComponentContent+"\nright footer PCII - Universidade do Minho | PG42857/PG42858\n\n@enduml";
        saveChart(theComponentContent);
    }

    private void saveChart(String content){
        try (PrintWriter out = new PrintWriter(Constants.chartFilename)) {
            out.println(content);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
