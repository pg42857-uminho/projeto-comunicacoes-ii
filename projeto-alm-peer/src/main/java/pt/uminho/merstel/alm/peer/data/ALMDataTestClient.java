package pt.uminho.merstel.alm.peer.data;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pt.uminho.merstel.alm.peer.agent.NeighbourManagementAgent;
import pt.uminho.merstel.alm.peer.agent.PeerAgent;
import pt.uminho.merstel.alm.peer.utils.Constants;
import pt.uminho.merstel.alm.peer.utils.NetworkUtils;
import pt.uminho.merstel.alm.peer.utils.SendMessage;

import java.net.InetAddress;
import java.util.Collection;
import java.util.Optional;
import java.util.Scanner;

public class ALMDataTestClient extends Thread {

    private static Logger logger = LogManager.getLogger(ALMDataTestClient.class);
    private static final long TIMER = 5000;

    private NeighbourManagementAgent neighbourManagementAgent;
    private final PeerAgent agent;

    public ALMDataTestClient(PeerAgent agent) {
        this.agent = agent;
        this.neighbourManagementAgent = agent.getNeighbourManagementAgent();
    }

    @Override
    public void run() {
        final Scanner in = new Scanner(System.in);

        while (true) {
            String line = "";

            Optional<InetAddress> optional = NetworkUtils.getLocalAddress();

            if (optional.isPresent()) {
                line = "Message from peer" + optional.get().getHostAddress();
            }
            else {
                line = "Message from peer XPTO";
            }

            byte[] array = line.getBytes();

            for (InetAddress inetAddress : neighbourManagementAgent.getConnections()) {
                SendMessage.getInstance().sendMessageUDP(inetAddress, Constants.DATA_PORT_NUMBER, array);
            }

            try {
                Thread.sleep(TIMER);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
