# Projeto Comunicacoes II

## Application-Layer Multicast (ALM)

Rede peer-to-peer, em que um peer envia uma mensagem para todos os restantes peers através de uma árvore de multicast, de modo a minimizar a quantidade de mensagens geradas.


## DOCKER INSTRUCTIONS

docker build . -t alm-image
docker network create alm-network
docker run --name rp-agent --network alm-network -dit alm-image
docker attach rp-agent 

// Alterar o ficheiro de configurações
// mvn clean package
// java -jar target/...
// CTRL+P + CTRL+Q

docker network inspect alm-network
// ver o endereço do rp-agent

docker run --name agent-1 --network alm-network -dit alm-image
docker attach agent-1
// Alterar o ficheiro de configurações
// echo '{"rp_address": "172.18.0.2", "is_rp":false }' > src/main/resources/peer-configuration.json
// mvn clean package
// java -jar target/...
// CTRL+P + CTRL+Q
